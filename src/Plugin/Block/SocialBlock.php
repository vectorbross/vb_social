<?php

namespace Drupal\vb_social\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'SocialBlock' block.
 *
 * @Block(
 *  id = "social_block",
 *  admin_label = @Translation("Social block"),
 * )
 */
class SocialBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['#attributes']['class'][] = 'social';
    $build['#attached']['library'][] = 'vb_social/social';

    $social_platforms = $this->getSocialPlatforms();

    foreach ($social_platforms as $key => $social_platform) {
      if (isset($this->configuration[$key]) && $this->configuration[$key] != '') {
        
        // Prepare url.
        if ($social_platform['type'] == 'email') {
          $url = Url::fromUri('mailto:' . $this->configuration[$key]);
        }
        else {
          $url = Url::fromUri($this->configuration[$key]);
        }

        $build[$key] = [
          '#type' => 'link',
          '#title' => $social_platform['title'],
          '#url' => $url,
          '#attributes' => [
            'class' => ['social__link', 'social__link--' . $key],
            'target' => '_blank'
          ]
        ];
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $social_platforms = $this->getSocialPlatforms();

    foreach ($social_platforms as $key => $social_platform) {
      $form[$key] = [
        '#type' => $social_platform['type'],
        '#title' => $social_platform['title'],
        '#default_value' => isset($config[$key]) ? $config[$key] : '',
        '#description' => isset($social_platform['description']) ? $social_platform['description'] : FALSE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $social_platforms = $this->getSocialPlatforms();

    foreach ($social_platforms as $key => $social_platform) {
      $this->configuration[$key] = $values[$key];
    }
  }

  /**
   * Returns the available social platforms.
   *
   * @return array[]
   */
  protected function getSocialPlatforms() {
    $social_platforms = [
      'facebook' => [
        'type' => 'url',
        'title' => $this->t('Facebook'),
      ],
      'twitter' => [
        'type' => 'url',
        'title' => $this->t('Twitter'),
      ],
      'instagram' => [
        'type' => 'url',
        'title' => $this->t('Instagram'),
      ],
      'pinterest' => [
        'type' => 'url',
        'title' => $this->t('Pinterest'),
      ],
      'youtube' => [
        'type' => 'url',
        'title' => $this->t('Youtube'),
      ],
      'linkedin' => [
        'type' => 'url',
        'title' => $this->t('LinkedIn'),
      ],
      'tiktok' => [
        'type' => 'url',
        'title' => $this->t('TikTok'),
      ],
      'email' => [
        'type' => 'email',
        'title' => $this->t('Email'),
        'description' => $this->t('This will render a simple mailto-link without subject or body.'),
      ],
      'extra' => [
        'type' => 'url',
        'title' => $this->t('Extra'),
        'description' => $this->t('The extra field can be used to insert links to platforms that aren\'t listed')
      ],
    ];

    return $social_platforms;
  }

}
